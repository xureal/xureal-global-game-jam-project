# Global Game Jam Project #

This repository will hold the code for the UI of the Elevux Global Game Jam project. This project is a React application that loads in a Unity application in an iframe.

to install, run `yarn`
to start, run `yarn start`

You will need to add in playfab credentials and a source for the 3d environment in then env vars