import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import './index.css';
import AppRouter from './AppRouter';
import AppContext from './AppContext';

ReactDOM.render(
  <React.StrictMode>
    <AppContext>
      <BrowserRouter>
        <AppRouter />
      </BrowserRouter>
    </AppContext>
  </React.StrictMode>,
  document.getElementById('root')
);
