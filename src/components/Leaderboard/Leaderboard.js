import React, { useEffect } from 'react';
import { useApp } from 'AppContext';
import styled from 'styled-components';
import PlayAgainButton from '../../assets/images/playagain.png';

const LeaderboardStyles = {
  Box: styled.div`
    font-family: 'Rowdies', cursive;
    font-size: 20px;
    display: 'flex';
    height: '90vh';
    width: '90vw';
    position: 'absolute';
    top: '50px';
    left: '75px';
    color: 'white';
  `,

  Container: styled.div`
    font-family: 'Rowdies', cursive;
    display: flex;
    flex-direction: column;
    justify-content: center;
    margin: 80px 15px 30px;
    height: 150px;
    width: 250px;
    border-radius: 15px;
    background-color: rgba(1, 1, 1, 0.9);
  `,
  Row: styled.div`
    font-family: 'Rowdies', cursive;
    display: flex;
    justify-content: space-between;
    align-items: center;
    width: 90%;
    height: 50px;
    border-bottom: 1px gray solid;
    margin: 3px auto;
  `,
  OverflowBody: styled.div`
    overflow-y: scroll;
    height: 400px;
    ::-webkit-scrollbar {
      display: none;
    }
    -ms-overflow-style: none;
    scrollbar-width: none;
  `,
};

function Leaderboard({ handleRestart }) {
  const { leaderboardData, gameData, getLeaderboardData, getHighScoreData } = useApp();

  useEffect(() => {
    if (leaderboardData.length === 0) getLeaderboardData();
    if (gameData.hiscore === 0) getHighScoreData()
  }, [getLeaderboardData, getHighScoreData]);

  return (
    <div
      style={{
        display: 'flex',
        height: '90vh',
        width: '90vw',
        position: 'absolute',
        top: '50px',
        left: '75px',
        color: 'white',
        textAlign: 'center',
      }}
    >
      <div
        id='score-leaderboard-container'
        style={{
          height: '100%',
          width: '55%',
          backgroundColor: 'rgba(1, 1, 1, 0.7)',
          borderRadius: '15px 0 0 15px',
        }}
      >
        <div
          id='score-display'
          style={{
            display: 'flex',
            justifyContent: 'center',
          }}
        >
          <LeaderboardStyles.Container>
            <h1>Your score</h1>
            <h2>{gameData.score}</h2>
          </LeaderboardStyles.Container>
          <LeaderboardStyles.Container>
            <h1>High score</h1>
            <h2>{gameData.hiscore ? gameData.hiscore : '0'}</h2>
          </LeaderboardStyles.Container>
        </div>
        <div style={{ fontStyle: 'italic bold', fontSize: '30px' }}>
          LEADERBOARD
        </div>
        <LeaderboardStyles.OverflowBody
          id='leaderboard-list'
          style={{ minHeight: '400px', fontStyle: 'italic' }}
        >
          <div id='leaderboardBody'>
            {leaderboardData.length > 0 &&
              leaderboardData.map((data, index) => {
                return (
                  <LeaderboardStyles.Row>
                    <span style={{ minWidth: '120px' }} id='placement'>
                      {index + 1}
                    </span>
                    <span style={{ minWidth: '220px' }}>
                      {data.DisplayName || 'N/A'}
                    </span>
                    <span
                      style={{
                        minWidth: '80px',
                        textAlign: 'right',
                        marginRight: '60px',
                      }}
                    >
                      {data.StatValue.toLocaleString()}
                    </span>
                  </LeaderboardStyles.Row>
                );
              })}
          </div>
        </LeaderboardStyles.OverflowBody>

        <img
          onClick={handleRestart}
          style={{
            maxWidth: '300px',
            margin: '40px auto',
            cursor: 'pointer',
          }}
          src={PlayAgainButton}
          alt='Play again'
        />
      </div>

      <div
        id='credit-box'
        style={{
          height: '100%',
          fontFamily: 'Rowdies',
          width: '45%',
          backgroundColor: 'rgba(1, 1, 1, 0.9)',
          borderRadius: '0 15px 15px 0',
        }}
      >
        <div style={{ marginTop: '80px', fontSize: '32px' }}>CREDITS</div>
        <div id='credits-body'>
          <div>
            <h3 style={{ marginTop: '30px' }}>John Correia</h3>
            <h5 style={{ marginTop: '2px' }}>GAME DESIGNER &amp; DEVELOPER</h5>
          </div>
          <div>
            <h3 style={{ marginTop: '30px' }}>Rex Cartegena</h3>
            <h5 style={{ marginTop: '2px' }}>
              SYSTEMS DEVELOPER &amp; COMPOSER
            </h5>
          </div>
          <div>
            <h3 style={{ marginTop: '30px' }}>Shaun Mardones</h3>
            <h5 style={{ marginTop: '2px' }}>
              WEB DEVELOPMENT &amp; LEVEL DESIGN
            </h5>
          </div>
          <div>
            <h3 style={{ marginTop: '30px' }}>Michael Marte</h3>
            <h5 style={{ marginTop: '2px' }}>3D CHARACTER ARTIST</h5>
          </div>
          <div>
            <h3 style={{ marginTop: '30px' }}>Kristin Busitzky</h3>
            <h5 style={{ marginTop: '2px' }}>
              GAME/UI DESIGNER &amp; CONCEPT ARTIST
            </h5>
          </div>
          <div>
            <h3 style={{ marginTop: '30px' }}>Joe Losgar</h3>
            <h5 style={{ marginTop: '2px' }}>SOUND DESIGN</h5>
          </div>
          <div>
            <h3 style={{ marginTop: '30px' }}>Hassan Seguias</h3>
            <h5 style={{ marginTop: '2px' }}>CREATIVE DIRECTION</h5>
          </div>
          <div>
            <h3 style={{ marginTop: '30px' }}>Esteban Bravo</h3>
            <h5 style={{ marginTop: '2px' }}>PROJECT MANAGEMENT</h5>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Leaderboard;
