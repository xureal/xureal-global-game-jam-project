import React from 'react';
import styled from 'styled-components';
import { useApp } from '../../AppContext';
import HourglassIcon from '../../assets/images/icons/Hourglass.png';

const InterfaceStyles = {
  Box: styled.div`
    font-family: 'Rowdies', cursive;
    font-size: 20px;
    padding: 12px 16px;
    margin: 32px 42px;
    border-radius: 15px;
    background-color: rgba(1, 1, 1, 0.7);
    color: white;
    transition: all ease 2s;
    min-width: 140px;
    display: flex;
    justify-content: center;
  `,
};

function UpperInterface() {
  const { gameData, getLeaderboardData } = useApp();
  return (
    <div
      id='upperContainer'
      style={{
        height: 'auto',
        width: '100vw',
        position: 'absolute',
        top: '0',
        left: '0',
        background: 'none',
      }}
    >
      <div
        style={{
          display: 'flex',
          justifyContent: 'space-between',
          alignItems: 'center',
        }}
      >
        <InterfaceStyles.Box onClick={getLeaderboardData}>
          <div >{gameData.score.toLocaleString()}</div>
        </InterfaceStyles.Box>
        {gameData.timer && (
          <InterfaceStyles.Box>
            <img src={HourglassIcon} style={{height: '30px', marginRight: '15px'}} alt=""/>
            {gameData.timer || '0:00'}</InterfaceStyles.Box>
        )}
      </div>
    </div>
  );
}

export default UpperInterface;
