import React from 'react';
// import Leaderboard from 'components/Leaderboard';

function GameOver() {
  //
  return (
    <div>
      <h1>Game Over</h1>
      <div>
        <Leaderboard />
      </div>
    </div>
  );
}

export default GameOver;
