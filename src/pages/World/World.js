import React, { useCallback, useEffect, useRef, useState } from 'react';
import { useApp } from 'AppContext';
import styled from 'styled-components';
import UpperInterface from '../../components/Interface/UpperInterface';
import Leaderboard from '../../components/Leaderboard';
import LogoImage from '../../assets/images/logo.png';
import PlayImage from '../../assets/images/play.png';
import InstructionsImage from '../../assets/images/instructions.png';
// import GameOver from 'pages/GameOver';

const LandingStyles = {
  Overlay: styled.div`
    height: 100vh;
    width: 100vw;
    overflow: hidden;
  `,
  Root: styled.div`
    position: absolute;
    background: none;
    z-index: 99;
    font-family: 'Poppins';
    height: 100vh;
    width: 100vw;
    display: flex;
    justify-content: center;
    align-items: center;
  `,
  Box: styled.div`
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    transition: all ease 2s;
  `,
  Hero: styled.div``,
  HeroImage: styled.img`
    max-width: 1024px;
    width: 60%;
    height: auto;
    margin: auto;
    margin-bottom: 64px;
  `,
  Login: styled.div``,
  InputTitle: styled.div`
    font-family: 'Rowdies', 'Poppins';
    color: white;
    font-size: 20px;
    margin-bottom: 16px;
  `,
  Input: styled.input`
    background-color: #000000dd;
    color: white;
    font-family: 'Rowdies', 'Poppins';
    font-size: 24px;
    text-align: center;
    border-radius: 24px;
    border: 1px solid #000000aa;
    width: 454px;
    padding: 16px 64px;
    margin-bottom: 32px;
  `,
  PlayButton: styled.div``,
  PlayIcon: styled.img`
    max-width: 240px;
    height: auto;
    cursor: pointer;
  `,
  Instructions: styled.img`
    height: auto;
    width: auto;
    max-height: 90vh;
    max-width: 90vw;
    z-index: 99;
    cursor: pointer;
  `,
};

function World() {
  const {
    authorized,
    setAuthorized,
    setUserData,
    unityLoaded,
    getLeaderboardData,
    getHighScoreData,
    gameOver,
    setGameOver
  } = useApp();
  const [username, setUsername] = useState('');
  const [usernameError, setUsernameError] = useState('');
  const meetingWorldRef = useRef(null);
  const [showInstructions, setShowInstructions] = useState(false);

  const WORLD_URL = process.env.REACT_APP_WORLD_URL;

  useEffect(() => {
    meetingWorldRef.current?.focus();
    if (usernameError) {
      setTimeout(() => {
        setUsernameError('');
      }, 2000);
    }
  }, [usernameError]);

  const handleChange = useCallback(
    (event) => {
      setUsername(event.target.value);
    },
    [setUsername]
  );

  const SendUnityMessage = (message) => {
    meetingWorldRef?.current?.contentWindow.postMessage(message, '*');
  };

  const handleSubmit = useCallback(() => {
    if (!username) {
      setUsernameError('Please enter a username');
      return;
    }
    if (username) {
      setUserData((prev) => ({ ...prev, username }));
      const loginCB = (data) => {
        if (data.success) {
          console.log('Logged in: ' + data.name);
          getLeaderboardData();
          getHighScoreData();
        } else {
          console.log('Playfab Login Error');
        }
      };
      window.gameData.logPlayer(username, loginCB);
      setShowInstructions(true);
      setAuthorized(true);
    }
  }, [
    setUserData,
    username,
    getHighScoreData,
    getLeaderboardData,
    setShowInstructions,
    setAuthorized
  ]);

  const handleStart = useCallback(() => {
    const message = JSON.stringify({
      unityComponent: 'GameManager',
      componentFunction: 'LoadLevel',
      message: 'test',
    });
    SendUnityMessage(message);
    setShowInstructions(false);
    meetingWorldRef.current?.focus();
  }, []);

  const handleRestart = useCallback(() => {
    const message = JSON.stringify({
      unityComponent: 'GameManager',
      componentFunction: 'RestartLevel',
      message: 'test',
    });
    SendUnityMessage(message);
    setShowInstructions(false);
    setGameOver(false)
    meetingWorldRef.current?.focus();
  }, [setGameOver]);

  return (
    <>
      {!authorized && !showInstructions && unityLoaded && (
        <LandingStyles.Root>
          <LandingStyles.Box>
            <LandingStyles.Hero>
              <LandingStyles.HeroImage src={LogoImage} alt='' />
            </LandingStyles.Hero>

            <LandingStyles.InputTitle>
              {'ENTER YOUR NAME'}
            </LandingStyles.InputTitle>
            {usernameError && <div>{usernameError}</div>}
            <LandingStyles.Input value={username} onChange={handleChange} />
            <LandingStyles.PlayButton>
              <LandingStyles.PlayIcon
                src={PlayImage}
                onClick={handleSubmit}
                alt=''
              />
            </LandingStyles.PlayButton>
          </LandingStyles.Box>
        </LandingStyles.Root>
      )}
      { showInstructions && authorized && (
        <LandingStyles.Root>
          <LandingStyles.Instructions
            onClick={handleStart}
            src={InstructionsImage}
            alt=''
          />
        </LandingStyles.Root>
      )}
      {authorized && !gameOver && <UpperInterface />}
      {gameOver && <Leaderboard handleRestart={handleRestart} />}
      <LandingStyles.Overlay>
        <iframe
          id='room-content-iframe'
          title='unityMeeting'
          src={WORLD_URL}
          width='100%'
          height='100%'
          frameBorder='0'
          sandbox='allow-forms allow-scripts allow-same-origin allow-popups allow-popups-to-escape-sandbox allow-modals allow-pointer-lock'
          ref={meetingWorldRef}
        ></iframe>
      </LandingStyles.Overlay>
    </>
  );
}

export default World;
