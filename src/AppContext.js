import {
  createContext,
  useState,
  useContext,
  useEffect,
  useCallback,
} from 'react';

export const AppCtx = createContext({});

const ApplicationProvider = ({ children }) => {
  const [authorized, setAuthorized] = useState(false);
  const [userData, setUserData] = useState({ username: '' });
  const [gameOver, setGameOver] = useState(false);
  const [leaderboardData, setLeaderboardData] = useState([]);

  // We can use this state to populate the top UI
  const [gameData, setGameData] = useState({
    score: 0,
    multiplier: 1,
    timer: 0,
    balance: 50,
    hiscore: 0,
    playerHP: 100,
    playerID: '',
    playerName: '',
    highScore: 0
  });
  const [unityLoaded, setUnityLoaded] = useState(false);

  const getLeaderboardData = useCallback(() => {
    window.gameData.getLeaderboardData((e) => {
      console.log('Leaderboard check', e);
      setLeaderboardData(e);
    });
  }, [setLeaderboardData]);

  const getHighScoreData = useCallback(() => {
    window.gameData.getHighscoreData((e) => {
      console.log('Highscore Number: ', e);
      setGameData((prev) => ({
        ...prev,
        highScore: e,
      }));
    });
  }, [setGameData]);

  const onTimerUpdate = useCallback((e) => {
    var date = new Date(null);
    date.setSeconds(e.detail.timerLimit - e.detail.timer);
    var formattedTime = date.toISOString().slice(14, 19);
    setGameData((prev) => ({
      ...prev,
      timer: formattedTime,
    }));
  }, []);

  useEffect(() => {
    const {
      REACT_APP_ENV_NAME,
      REACT_APP_ENV_TITLE,
      REACT_APP_ENV_SECRET,
      REACT_APP_ENV_TYPE,
    } = process.env;

    const logCreds = {
      name: REACT_APP_ENV_NAME,
      type: REACT_APP_ENV_TYPE,
      title: REACT_APP_ENV_TITLE,
      secret: REACT_APP_ENV_SECRET
    };
    console.log('creds', logCreds)
    window.gameData.init(logCreds);

    const onMessageListener = (event) => {
      try {
        console.log(event.data)
        const data =
          typeof event.data === 'string' ? JSON.parse(event.data) : event.data;
        if (data.type === 'game_event') {
          if (data.action === 'loaded') setUnityLoaded(true);
          if (data.action === 'game_end') {
            setTimeout(() => {
              setGameOver(true);
            }, 3500)
          }
          if (data.action === 'game_start') {
            // here we can control whatever we need for timers, resets, etc
            console.log('game started');
          }
          console.log(
            '%cReact Event Listener',
            'color: green; font-weight: bold; font-size: 14px;',
            data
          );
        }
      } catch (err) {
        if (err) console.error('Listener Error:', err, event.data);
      }
    };

    const onGameDataUpdate = (e) => {
      if (!e.detail.action) return;
      const receivedData = window.gameData.getAll();
      console.log(
        '%cAction Recieved: ' + e.detail.action + ' Game Data Recieved:',
        'color: green; font-weight: bold; font-size: 12px;',
        receivedData
      );
      // here we can control whatever is needed in the UI
      setGameData((prev) => ({
        ...prev,
        score: receivedData.score || prev.score,
        multiplier: receivedData.multiplier || prev.multiplier,
        balance: receivedData.balance || prev.balance,
        hiscore: receivedData.hiscore || prev.hiscore,
        playerHP: receivedData.playerHP || prev.playerHP,
        playerID: receivedData.playerID || prev.playerID,
        playerName: receivedData.playerName || prev.playerName,
      }));
    };

    document.addEventListener('gameDataUpdates', onGameDataUpdate);
    document.addEventListener('gameDataTimerUpdate', onTimerUpdate);
    window.onmessage = onMessageListener;

    return () => {
      window.onmessage = null;
      document.removeEventListener('gameDataUpdates', onGameDataUpdate);
      document.removeEventListener('gameDataTimerUpdate', onTimerUpdate);
    };
  }, [onTimerUpdate]);

  // useEffect(() => {
  //   if (iframeLoaded) {
  //     // handle logic needed after iframe is loaded
  //   }
  // }, [iframeLoaded]);

  return (
    <AppCtx.Provider
      value={{
        authorized,
        setAuthorized,
        userData,
        setUserData,
        unityLoaded,
        gameData,
        leaderboardData,
        getLeaderboardData,
        getHighScoreData,
        gameOver,
        setGameOver
      }}
    >
      {children}
    </AppCtx.Provider>
  );
};

export const useApp = () => {
  return useContext(AppCtx);
};

export default ApplicationProvider;
