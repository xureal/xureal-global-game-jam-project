import React from "react";
import { Routes, Route } from "react-router-dom";
// import { useApp } from "AppContext";
// import Auth from './pages/Auth';
import World from './pages/World';
// import GameOver from './pages/GameOver';

function App() {
  return (
    <div>
      <Routes>
        <Route path='/' element={<World />} />
        {/* <Route path='/gameover' element={<GameOver />} /> */}
      </Routes>
    </div>
  );
}

export default App;
