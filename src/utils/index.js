export const capitalizeString = (inputString) => {
  if (typeof inputString !== 'string') return inputString;
  return inputString[0].toUpperCase() + inputString.slice(1).toLowerCase();
};
