export const sizes = {
  mobileS: '320px',
  mobileM: '375px',
  mobileL: '425px',
  tablet: '768px',
  desktopS: '1024px',
  desktopM: '1280px',
  desktopL: '1440px',
};

export const devices = {
  mobileS: `(max-width: ${sizes.mobileS})`,
  mobileM: `(max-width: ${sizes.mobileM})`,
  mobileL: `(max-width: ${sizes.mobileL})`,
  tablet: `(max-width: ${sizes.tablet})`,
  desktopS: `(max-width: ${sizes.desktopS})`,
  desktopM: `(max-width: ${sizes.desktopM})`,
  desktopL: `(max-width: ${sizes.desktopL})`,
};
